# Módulos de Webpack

## Índice

1. [Primeros pasos](#firstSteps)
2. [Primer bundle](#firstBundle)
3. [Loaders](#loaders)
4. [Versión de los módulos](#moduleVersions)


## Primeros pasos <a name="firstSteps"></a>

En primer lugar, hay que crear un repositorio git en la carpeta del proyecto. Para ello se usa el siguiente comando:

```
git init
```

***Nota:** para ver más acerca de cómo usar git, visita **[cómo usar git](https://gist.github.com/mindplace/b4b094157d7a3be6afd2c96370d39fad)**.*

Una vez inicializado y vinculado el repositorio, se tiene que crear la carpeta `src` y `dist`. A continuación, se necesita **[nodejs](https://nodejs.org/en/)** en el dispositivo. En caso que el terminal ya disponga de **nodejs** instalado, este paso puede ser omitido y, además, se puede comprobar la versión de éste mediante el siguiente comando:

```
node -v
```

Asimismo, también se va a requerir tener instalado **[npm](https://www.npmjs.com/)**, el cual se va a instalar con el mismo **nodejs**. Una vez más, se puede comprobar la versión de la siguiente manera:

```
npm -v
```

Una vez instalados los requisitos, ya se puede inicializar **npm**:

```
npm init -y
```

De esta forma, se genera el archivo `package.json` (archivo que contiene ciertos atributos que a continuación serán citados y explicados).

* ***name***: contiene el nombre del proyecto que se va a desarrollar para poder identificarlo. No puede excederse de los 214 caracteres.
* ***version***: haciendo uso del formato *x.x.x*, se usa para llevar un registro de todas las modificaciones del proyecto.
* ***description***: opcionalmente, se puede añadir una descripción del proyecto.
* ***main***: es el *entry point* del proyecto. Es donde la aplicación buscará para exportar los módulos. Normalmente suele ser modificado desde un archivo de configuración.
* ***scripts***: permite configurar comandos personalizados haciendo el manejo de éstos mucho más senzillo.
* ***keywords***: palabras clave del proyecto que se va a desarrollar para poder etiquetarlo.
* ***author***: propietario o propietarios del proyecto.
* ***licence***: indica el tipo de licencia del proyecto. Por defecto es *ISC*, de uso privado. Así pues, ese código pasa a tener copyright del autor o autores del mismo. Por otro lado, existe el tipo de licencia *MIT*, de uso libre. Con este último, cualquier persona puede hacer uso de ese código sin la necesidad de obtener el consentimiento del autor o autores. Eventualmente, podría hacer uso comercial de ese código. Para obtener más información sobre licencias, visita **[OpenSource Licenses](https://opensource.org/licenses/)**.

Gracias a la opción `-y` del comando, se generan los atributos que se acaban de citar de forma genérica. Éstos pueden ser modificados en el mismo archivo o bien introducidos manualmente uno a uno mediante la consola eliminando la opción `-y` del comando.

Finalmente, una vez configurado todo el entorno, se instalan los primeros paquetes del proyecto:

```
npm install webpack webpack-cli --save-dev
```

Con esta sintaxis, los paquetes se descargan y se almacenan dentro del proyecto. Éstos se almacenan a su vez en el archivo `package.json`, generando un nuevo atributo llamado ***devDependencies***, un objeto que almacenará todos los módulos instalados como dependencia de desarrollo, puesto que el módulo `webpack` y `webpack-cli` solo se va a utilitzar en desarrollo y no en producción. Para comprobar que se han instalado de forma correcta, se puede verificar de la misma forma que se ha hecho anteriormente, mediante el comando de la versión:

```
webpack -v
```

En caso que aparezca que el comando no existe, querrá decir que el módulo no está instalado a nivel global, pero si en el proyecto. Para verificar eso, se puede hacer de la siguiente forma:

```
npm list webpack
```

Por otro lado, si, además, se quieren instalar los paquetes de forma global, se puede hacer mediante el siguiente comando:

```
npm install webpack -g
```

Una vez instalado, se genera un archivo llamado `package-lock.json` en la raíz que se va a ignorar por ahora. También se puede apreciar que ha aparecido una nueva carpeta llamada `node_modules` (la encargada de almacenar todos esos módulos), la cual se recomienda añadir en el `.gitignore` mediante la siguiente sintaxis:

```
# Don't track these folders
node_modules/
```

***Nota:** en caso de no tener archivo `.gitignore`, hay que crearlo en la carpeta raíz.*

Dentro de cada uno de los elementos del objeto ***devDependencies***, aparece la versión con la que se ha instalado el módulo. Además, viene precedido por un caracter especial que determina si, al instalar este proyecto de nuevo, se tiene que actualizar este módulo a la versión más reciente o no. Si por lo contrario se quiere conservar esa versión, tan solo hay que eliminar el caracter `^` de cada una de las dependencias. Algunas veces pueden existir incompatibilidades entre módulos debido a las nuevas versiones de estos paquetes. Por ese motivo, al final de esta documentación se van a citar las versies que se usarán en cada uno de los módulos, generando así una versión estable para su uso. Para instalar una versión concreta en vez de la más actual, se puede usar el siguiente comando modificando los números de la versión por la deseada:

```
npm install webpack@4.30.0 --save-dev
```

En caso de necesitar eliminar el paquete por alguna razón, se puede utilizar el comando:

```
npm uninstall webpack --save-dev
```

Antes de saltar al siguiente paso, es necesario conocer las otras opciones de instalación de **npm**. Hasta ahora, se ha instalado un paquete mediante la opción **--save-dev** para que se almacene a modo de dependencia de desarrollo (como se ha comentado anteriormente). No obstante, existen otras formas de almacenar los módulos:

* ***--save***: se almacena el módulo en el objeto **dependencies**, cuyo enumera todos los paquetes que se usan en producción.
* ***--save-optional***: se almacena el módulo en el objeto **optionalDependencies** y, como su nombre indica, guarda los módulos opcionales.
* ***--save-dev***: es el objeto que se ha usado hasta ahora, almacena los módulos en **devDependencies** y solo se usan en desarrollo.
* ***-no-save***: instala el paquete, pero no lo guarda en el archivo `package.json`.

De la misma forma que se usan estas opciones de comando para la instalación, pueden ser usadas para desinstalar como en el último ejemplo que se ha usado para desinstalar el paquete webpack.

## Primer bundle <a name="firstBundle"></a>

Un bundle es ese archivo que se genera al compilar todos los módulos que se tengan instalados y todo el código fuente que se haya creado. De esta forma, convierte todo nuestro proyecto en un único archivo listo para ser llamado mediante HTML. Un archivo bundle puede contener desde CSS, JS, HTML o incluso lenguajes algo más complejos como SASS convertido en javascript.

Para empezar, hay que crear un nuevo archivo llamado `index.js` dentro de la carpeta `src/js` (habrá que crear la carpeta `js`). En él, se puede poner el código javascript que se desee. Para empezar y a modo de comprobación:

```javascript
document.write("Hello World!");
```

A continuación, se crea un archivo llamado `webpack.config.js` en la raíz. Éste último contendrá toda la configuración de webpack de la siguiente forma:

```javascript
const path = require('path');

module.exports = {
    mode: "development",
    entry : path.resolve(__dirname, 'src/js/index.js'),
    output: {
        filename : 'main.js',
        path: path.resolve(__dirname, 'dist')
    }
};
```

Mediante la variable constante `path`, se consigue acceder a la ruta de la carpeta `dist`. Por otro lado, se exporta un objeto que contiene la información sobre el archivo de entrada y el de salida. Por ahora solo hay dos elementos dentro del objeto, más adelante se irán añadiendo más en función de las necesidades del proyecto.

* ***mode***: indica si el proyecto está en modo de desarrollo o de producción (`development` o `production`). La diferencia entre ellos es que el de desarrollo permite depurar el código mostrando los errores y, en consecuencia, suele pesar bastante más el `bundle` que en producción.
* ***entry***: es la ruta del archivo (incluyendo su nombre) que va a compilarse.
* ***output***: es un objeto que contiene el nombre del archivo que se va a exportar de la compilación en la ruta dónde va a ser almacenado.

Ahora es momento de generar el archivo HTML llamado `index.html` dentro de la carpeta raíz. En él se puede introducir un código parecido a este:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Webpack configuration</title>
</head>
<body>
    <script src="dist/js/main.js"></script>
</body>
</html>
```

Finalmente, se vuelve al archivo `package.json` para configurar un comando de compilación. En este ejemplo, se va a usar el comando **build** y el archivo completo queda de la siguiente forma:

```json
{
  "name": "webpack-configuration",
  "version": "1.0.0",
  "description": "Best configuration to start your own project.",
  "main": "main.js",
  "scripts": {
    "build": "webpack"
  },
  "keywords": [],
  "author": "",
  "license": "MIT",
  "devDependencies": {
    "webpack": "4.30.0",
    "webpack-cli": "3.3.2"
  }
}
```

Respecto el archivo predeterminado se ha modificado el atributo **main** por `main.js` y se ha añadido un nuevo elemento en **script** (el script **test** se puede eliminar, pero, en caso de dejarlo, es importante tener en cuenta que este archivo **json** está en modo estricto, lo cual quiere decir que el último elemento de cada objeto no tiene que terminar con `,`). En caso de querer modificar el nombre de la configuración de webpack o su ruta, se deberá de indicar mediante la siguiente sintaxis:

```json
{
    "scripts": {
        "build": "webpack --config ./{foldername}/{filename}.js"
    }
}
```

Para compilar, se introduce el siguiente comando y ya podrá ejecutarse el archivo `index.html` desde el navegador:

```
npm run build
```

## Loaders <a name="loaders"></a>

Mediante un **loader** (cargador si se traduce directamente) se pueden cargar diferentes archivos, como CSS, dentro del mismo bundle. Existen loaders para cargar casi cualquier tipo de archivo. En este caso, solo se van a tratar aquellos más frecuentes y más utilizados. Para cualquier otro tipo de archivo, se puede consultar la [lista de loaders](https://github.com/webpack/docs/wiki/list-of-loaders).

### CSS

En primer lugar, es necesario instalar aquellos módulos que van a servir para poder cargar e interpretar el lenguaje (mediante **css-loader**) y poder imprimir el estilo en el navegador (con **style-loader**). De este modo, hay que instalar los módulos guardándolos como dependencia de desarrollo:

```
npm install style-loader css-loader --save-dev
```

Una vez instalados, aparecerán en el elemento **devDependencies** del archivo `package.json`. No obstante, hace falta configurar el archivo de configuración (`webpack.config.js`) para que dichos módulos hagan la función citada anteriormente. Para configurarlos habrá que añadir un nuevo elemento dentro del objeto **module.exports** de la siguiente forma:

```javascript
module.exports = {
    // ...
    module: {
        rules: [
            // css Loader && style-loader
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    }
};
```

Como se puede apreciar, se ha añadido un nuevo elemento que contiene otro objeto: **module**. A su vez, el elemento **rules** es un array que contendrá cada uno de los casos que se quieran cargar mediante **loaders**. Dentro, habrá otro objeto con dos elementos más, **test** y **use**. El primero debe contener una expresión regular para determinar a qué archivos tienen que ser cargados mediante los loaders. Éstos últimos son el contenido del elemento **use** (en este caso aparece como un array ya que son dos, pero si fuera solo uno podría ser simplemente una cadena de texto).

Existe otro modo de cargar módulos dónde cada elemento del array de **use** contiene otro objeto. Si se quiere configurar de ese modo, la sintixis deberá ser de la siguiente manera:

```javascript
module.exports = {
    // ...
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            }
        ]
    }
};
```

***Nota**: es muy importante el orden de los loaders, de ponerlos mal, mostrará error al intentar interpretar el lenguaje*.

Una vez configurado, ya se puede crear el archivo con extensión .css para dar estilo a la página. En este caso, se va a modificar el color de fondo de la página, pero puede contener cualquier tipo de estilo (excepto [Sass](https://sass-lang.com/), que se verá a continuación). Se crea la carpeta `css` dentro de `src` y se genera un nuevo archivo llamado `main.css` con el siguiente contenido:

```css
body {
    background-color: coral;
}
```

### Sass

Para complicar ligeramente el proyecto y darnos facilidades como la posibilidad de escribir código **Sass**, se va a incorporar la posibilidad de interpretar dicho lenguaje. Primero de todo, se va a necesitar el nuevo módulo:

```
npm install sass-loader node-sass --save-dev
```

**Nota:**

*Al instalar el módulo **node-sass** existe la posibilidad que aparezca una vulnerabilidad. Ésta está relacionada con el módulo **tar**, ya que puede ser una versión demasiado antigua. Para solucionarlo, hay que abrir el archivo `package-lock.json` y buscar el siguiente término **"tar"**, incluyendo las comillas dobles. De este modo, se encontrarán los módulos que tienen al paquete **tar** como dependencia. Aquellos que tengan una versión inferior a la 4.4.2 deberán ser sustituidos por la 4.4.8. Además habrá que buscar el siguiente código:*

```javascript
"version": "2.2.1",
"resolved": "https://registry.npmjs.org/tar/-/tar-2.2.1.tgz",
"integrity": "sha1-jk0qJWwOIYXGsYrWlK7JaLg8sdE=",
```

*Y modificarlo por*:

```
"version": "4.4.8",
"resolved": "https://registry.npmjs.org/tar/-/tar-4.4.8.tgz",
"integrity": "sha512-LzHF64s5chPQQS0IYBn9IN5h3i98c12bo4NCO7e0sGM2llXQ3p2FGC5sdENN4cTW48O915Sh+x+EXx7XW96xYQ==",
```

Al finalizar, habrá que eliminar la carpeta `node_modules` y volverla a inicializar con la nueva configuración de la siguiente forma:

```
rm -r -fo node_modules
npm i
```

*Finalmente, se podrá comprobar que ya no existen vulnerabilidades mediante el siguiente comando*:

```
npm audit
```

*Fin de la nota*

Hecho esto, es momento de generar una nueva regla dentro del archivo de configuración (`webpack.config.js`):

```javascript
{
    test: /\.scss$/,
    use : [
        {
            loader: "style-loader" // load the style into the page
        },
        {
            loader: "css-loader" // interprete css language to js
        },
        {
            loader: "sass-loader" // compiles Sass to CSS
        }
    ]
}
```

Se crea una nueva carpeta en `src` llamada `scss` y se añade el nuevo archivo `main.scss` con el siguiente código:

```scss
$color: "coral";

body {
    background-color: $color;
}
```

Una vez creado, hay que modificar la ruta del archivo de estilo que se añadió previamente en `ìndex.js` por la nueva:

```javascript
import "./scss/main.scss";
```

Al finalizar, es momento de compilar y se mostrará el nuevo estilo realizado con **Sass** en el `index.html`.

## Versión de los módulos <a name="moduleVersions"></a>

* **webpack**: 4.30.0
* **webpack-cli**: 3.3.2
* **css-loader**: 2.1.1
* **style-loader**: 0.23.1
* **sass-loader**: 7.1.0
* **node-sass**: 4.12.0